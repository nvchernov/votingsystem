﻿using DevExpress.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VotingCore.DataModels;

namespace BestVoting.VotingSchemes
{
    public class VotingsViewModel : ViewModelBase
    {

        public VotingsViewModel(List<Voting> votings)
        {
            m_Votings = new ObservableCollection<Voting>(votings);
        }

        private ObservableCollection<Voting> m_Votings;
        public ObservableCollection<Voting> Votings
        {
            get => m_Votings;
            set
            {
                m_Votings = value;
                RaisePropertyChanged("Votings");
            }
        }

        private Voting m_SelectedVoting;
        public Voting SelectedVoting
        {
            get => m_SelectedVoting;
            set
            {
                m_SelectedVoting = value;
                wnd.Close();
            }
        }

        private VotingsWindow wnd = null;
        public bool? ShowDialog()
        {
            wnd = new VotingsWindow() { DataContext = this };

            return wnd.ShowDialog();
        }

    }
}
