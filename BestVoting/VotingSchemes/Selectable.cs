﻿using DevExpress.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VotingCore.DataModels;

namespace BestVoting.VotingSchemes
{
    public class Selectable<T> : ViewModelBase
    {
        private T m_Item;
        public T Item
        {
            get => m_Item;
            set
            {
                m_Item = value;
            }
        }

        public Selectable(T item)
        {
            m_Item = item;
        }

        private bool m_Checked = false;
        public bool Checked
        {
            get => m_Checked;
            set
            {
                m_Checked = value;
                RaisePropertyChanged("Checked");
            }
        }
    }
}
