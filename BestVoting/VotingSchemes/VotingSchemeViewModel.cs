﻿using DevExpress.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VotingCore;
using VotingCore.DataModels;
using VotingCore.Helpers;
using System.Data.Entity;
using System.Windows;

namespace BestVoting.VotingSchemes
{
    public class VotingSchemeViewModel : ViewModelBase
    {
        VotingDb ctx = new VotingDb();

        /// <summary>
        /// ViewModel для схемы голосования
        /// </summary>
        /// <param name="votingScheme">Схема голосования</param>
        /// <param name="readOnly">Форма будет read-only</param>
        public VotingSchemeViewModel(VotingScheme votingScheme, bool readOnly = true)
        {
            m_VotingScheme = votingScheme;

            RefreshMembersByFilter();

            Enabled = readOnly;
        }

        private VotingScheme m_VotingScheme;
        public VotingScheme VotingScheme
        {
            get => m_VotingScheme;
            set
            {
                m_VotingScheme = value;
            }
        }

        private ObservableCollection<Selectable<Group>> m_Groups;
        public ObservableCollection<Selectable<Group>> Groups
        {
            get => m_Groups;
            set
            {
                m_Groups = value;

                foreach (var item in m_Groups)
                    item.PropertyChanged += FilterGroupChanged;

                RaisePropertyChanged("Groups");
            }
        }

        private bool m_Enabled = false;
        public bool Enabled
        {
            get => m_Enabled;
            set
            {
                m_Enabled = value;
                RaisePropertyChanged("Enabled");
            }
        }

        /// <summary>
        /// Обновляет участников по фильтрам
        /// </summary>
        private void RefreshMembersByFilter()
        {
            var request = ctx.Members.Include(x => x.Group).AsQueryable();

            if (m_CatAgeFilter != null)
                request = request.Where(x => x.CatAge == CatAgeFilter);

            if (m_CastrateFilter != null)
                request = request.Where(x => x.Castrate == m_CastrateFilter);

            if (m_SexFilter != null)
                request = request.Where(x => x.Sex == m_SexFilter);

            if(m_Groups?.Any(x => x.Checked) ?? false)
            {
                var selectedGroupsIds = m_Groups.Where(x => x.Checked).Select(x => x.Item.Id).ToList();
                request = request.Where(x => selectedGroupsIds.Contains(x.GroupId));
            }

            request = request.OrderBy(x => x.Number).AsQueryable();

            VotingSchemeMembers =
                new ObservableCollection<Selectable<Member>>(request.ToList().Select(x => new Selectable<Member>(x)));

        }

        private VotingSchemeWindow wnd;
        public bool? ShowDialog()
        {
            wnd = new VotingSchemeWindow() { DataContext = this };
            return wnd.ShowDialog();
        }

        private ObservableCollection<Selectable<Member>> m_VotingSchemeMembers;
        public ObservableCollection<Selectable<Member>> VotingSchemeMembers
        {
            get => m_VotingSchemeMembers;
            set
            {
                m_VotingSchemeMembers = value;
                RaisePropertyChanged("VotingSchemeMembers");
            }
        }

        private IEnumerable<KeyValuePair<Enum, string>> m_VotingSchemeTypes;
        public IEnumerable<KeyValuePair<Enum, string>> VotingSchemeTypes
        {
            get => m_VotingSchemeTypes ?? (m_VotingSchemeTypes = EnumExt.ConvertToKeyValueList(typeof(VotingType)));
            set
            {
                m_VotingSchemeTypes = value;
                RaisePropertyChanged("VotingSchemeTypes");
            }
        }

        private VotingType m_SelectedSchemeType;
        public VotingType SelectedSchemeType
        {
            get => m_SelectedSchemeType;
            set
            {
                m_SelectedSchemeType = value;
                RaisePropertyChanged("SelectedSchemeType");
            }
        }

        private void FilterGroupChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            RefreshMembersByFilter();
        }

        public bool? MembersSelection
        {
            get => false;
            set
            {
                MemebrsChangeSelection();
            }
        }
        private void MemebrsChangeSelection()
        {
            if (m_VotingSchemeMembers == null || m_VotingSchemeMembers.Count == 0)
                return;

            var first = m_VotingSchemeMembers.First();

            if (first == null)
                return;

            var val = !first.Checked;

            foreach (var item in m_VotingSchemeMembers)
                item.Checked = val;
            
            RaisePropertyChanged("VotingSchemeMembers");
        }

        #region Filters

        private Sex? m_SexFilter;
        public Sex? SexFilter
        {
            get => m_SexFilter;
            set
            {
                m_SexFilter = value;
                RaisePropertyChanged("SexFilter");
                RefreshMembersByFilter();
            }
        }

        private IEnumerable<KeyValuePair<Enum, string>> m_Sexes;
        public IEnumerable<KeyValuePair<Enum, string>> Sexes
        {
            get => m_Sexes ?? (m_Sexes = EnumExt.ConvertToKeyValueListWithEmpty(typeof(Sex)));
            set
            {
                m_Sexes = value;
                RaisePropertyChanged("Sexes");
            }
        }

        private CatAge? m_CatAgeFilter;
        public CatAge? CatAgeFilter
        {
            get => m_CatAgeFilter;
            set
            {
                m_CatAgeFilter = value;
                RaisePropertyChanged("CatAgeFilter");
                RefreshMembersByFilter();
            }
        }

        private IEnumerable<KeyValuePair<Enum, string>> m_Ages;
        public IEnumerable<KeyValuePair<Enum, string>> Ages
        {
            get => m_Ages ?? (m_Ages = EnumExt.ConvertToKeyValueListWithEmpty(typeof(CatAge)));
            set
            {
                m_Ages = value;
                RaisePropertyChanged("Ages");
            }
        }

        private bool? m_CastrateFilter = null;
        public bool? CastrateFilter
        {
            get => m_CastrateFilter;
            set
            {
                m_CastrateFilter = value;
                RaisePropertyChanged("CastrateFilter");
                RefreshMembersByFilter();
            }
        }

        #endregion

        #region OK\Cancel\Save

        private DelegateCommand m_OkCommand;
        public DelegateCommand OkCommand { get => m_OkCommand ?? (m_OkCommand = new DelegateCommand(Ok)); }

        private void Ok()
        {
            if(!Valid())
                return;

            if(m_Enabled)
            {
                Save();
                wnd.DialogResult = true;
            }
            
            wnd.Close();
        }

        private bool Valid()
        {
            if(m_VotingSchemeMembers == null || m_VotingSchemeMembers.Count == 0 || m_VotingSchemeMembers.Count(x => x.Checked) == 0)
            {
                MessageBox.Show("Не выбран ни один участник для голосования");
                return false;
            }

            if(string.IsNullOrWhiteSpace(m_VotingScheme.Name))
            {
                MessageBox.Show("Не задано имя для голосования");
                return false;
            }

            return true;
        }

        private void Save()
        {
            m_VotingScheme.Members = 
                m_VotingSchemeMembers.Where(x => x.Checked).Select(x => x.Item).ToList();

            ctx.VotingSchemes.Add(m_VotingScheme);
            ctx.SaveChanges();
        }

        private DelegateCommand m_CancelCommand;
        public DelegateCommand CancelCommand { get => m_CancelCommand ?? (m_CancelCommand = new DelegateCommand(Cancel)); }

        private void Cancel() => wnd?.Close();

        #endregion

    }
}
