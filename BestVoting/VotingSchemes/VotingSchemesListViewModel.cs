﻿using DevExpress.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using VotingCore;
using VotingCore.DataModels;
using BestVoting.VotingProcesses;
using System.Data.Entity;
using BestVoting.Reports;

namespace BestVoting.VotingSchemes
{
    public class VotingSchemesListViewModel : ViewModelBase
    {

        public VotingSchemesListViewModel()
        {
            m_Groups = ctx.Groups.ToList();
        }

        VotingSchemesListWindow wnd = null;
        VotingDb ctx = new VotingDb();

        public void ShowDialog()
        {
            wnd = new VotingSchemesListWindow() { DataContext = this };

            VotingSchemes = new ObservableCollection<VotingScheme>(ctx.VotingSchemes.ToList());

            wnd.ShowDialog();
        }

        private List<Group> m_Groups;
        

        private DelegateCommand m_CreateCommand;
        public DelegateCommand CreateCommand { get => m_CreateCommand ?? (m_CreateCommand = new DelegateCommand(Create)); }
        private void Create()
        {
            var voting = new VotingScheme();

            var votingWnd = new VotingSchemeViewModel(voting) { Groups = new ObservableCollection<Selectable<Group>>(m_Groups.Select(x => new Selectable<Group>(x))) };

            if (votingWnd.ShowDialog() == true)
            {
                VotingSchemes.Add(voting);
            }

        }

        private DelegateCommand m_PlayCommand;
        public DelegateCommand PlayCommand { get => m_PlayCommand ?? (m_PlayCommand = new DelegateCommand(Play)); }
        private void Play()
        {
            if (SelectedVotingScheme == null)
            {
                MessageBox.Show("Не выбран ни один элемент");
                return;
            }

            VotingProcessViewModel votingProc = new VotingProcessViewModel(m_SelectedVotingScheme.Id);
            votingProc.Start();
            votingProc.ShowDialog();
            votingProc.Stop();
        }


        private DelegateCommand m_ViewCommand;
        public DelegateCommand ViewCommand { get => m_ViewCommand ?? (m_ViewCommand = new DelegateCommand(View)); }
        private void View()
        {
            if(m_SelectedVotingScheme != null)
            {
                var votingVM = new VotingSchemeViewModel(m_SelectedVotingScheme, readOnly: false) { Groups = new ObservableCollection<Selectable<Group>>(m_Groups.Select(x => new Selectable<Group>(x))) };
                votingVM.ShowDialog();
            }
        }

        private DelegateCommand m_DeleteCommand;
        public DelegateCommand DeleteCommand { get => m_DeleteCommand ?? (m_DeleteCommand = new DelegateCommand(Delete)); }
        private void Delete()
        {
            if (SelectedVotingScheme == null)
            {
                MessageBox.Show("Не выбран ни один элемент");
                return;
            }

            if (MessageBox.Show("Вы действительно хотите удалить?", "Удаление", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                return;

            var editedVoting = ctx.Entry(m_SelectedVotingScheme).State = System.Data.Entity.EntityState.Deleted;

            ctx.SaveChanges();

            VotingSchemes.Remove(m_SelectedVotingScheme);
        }

        private DelegateCommand m_ShowReportCommand;
        public DelegateCommand ShowReportCommand { get => m_ShowReportCommand ?? (m_ShowReportCommand = new DelegateCommand(ShowReport)); }
        private void ShowReport()
        {
            if (m_SelectedVotingScheme == null)
            {
                MessageBox.Show("Не выбран ни один элемент");
                return;
            }

            var votings = ctx.Votings.Where(x => x.VotingSchemeId == m_SelectedVotingScheme.Id).ToList();

            if(votings == null || votings.Count == 0)
            {
                MessageBox.Show($"{m_SelectedVotingScheme.Name} не имеет ни одного голосования");
                return;
            }

            var votingsViewModel = new VotingsViewModel(votings);
            votingsViewModel.ShowDialog();

            if(votingsViewModel.SelectedVoting != null)
            {
                var report = new Report(votingsViewModel.SelectedVoting.Id);
                var reportVM = new ReportViewModel(report);

                reportVM.Show();
                reportVM.RenderReport();
            }

        }

        private ObservableCollection<VotingScheme> m_VotingSchemes;
        public ObservableCollection<VotingScheme> VotingSchemes
        {
            get => m_VotingSchemes;
            set
            {
                m_VotingSchemes = value;
                RaisePropertyChanged("VotingSchemes");
            }
        }

        private VotingScheme m_SelectedVotingScheme;
        public VotingScheme SelectedVotingScheme
        {
            get => m_SelectedVotingScheme;
            set
            {
                m_SelectedVotingScheme = value;
                RaisePropertyChanged("SelectedVotingScheme");
            }
        }





    }
}
