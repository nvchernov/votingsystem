﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VotingCore.DataModels;

namespace BestVoting.Reports
{
    public class MemberRating
    {
        private Member m_Member;
        public Member Member { get => m_Member; set => m_Member = value; }

        private List<int> m_Votes;
        public List<int> Votes { get => m_Votes; set => m_Votes = value; }

        private int SumVotesValues() => m_Votes.Sum(x => x);

        private int m_Sum;
        public int Sum { get => m_Sum == 0 ? (m_Sum = SumVotesValues()) : m_Sum; }
    }
}
