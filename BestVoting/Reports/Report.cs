﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VotingCore;
using VotingCore.DataModels;
using System.Data.Entity;
using VotingCore.Helpers;

namespace BestVoting.Reports
{
    public class Report
    {
        private readonly Voting m_Voting;

        VotingDb votingDb = new VotingDb();

        public Report(long votingId)
        {
            this.m_Voting = votingDb.Votings
                .Include(x => x.VotingScheme)
                .Include(x => x.Votes.Select(v => v.Member.Group))
                .FirstOrDefault(x => x.Id == votingId);
        }

        public Voting Voting { get => m_Voting; }

        public string Name { get => m_Voting.Name; }

        public VotingType VotingType { get => m_Voting.VotingScheme.VotingType; }

        public string VotingTypeString { get => m_Voting.VotingScheme.VotingType.Description(); }

        public DateTime Date { get => m_Voting.Date; }

        private List<MemberRating> m_Rating;
        public List<MemberRating> Rating { get => m_Rating ?? (m_Rating = SortMembersByVotes()); }

        public List<MemberRating> SortMembersByVotes()
        {
            var ratingRequest = m_Voting.Votes
                .GroupBy(x => x.Member, new EntityIdComparer())
                .Select(x => new MemberRating()
                {
                    Member = (Member)x.Key,
                    Votes = x
                        .Where(v => v.ValueAsInt() != int.MinValue)
                        .Where(v => v.MemberId != null)
                        .Select(v => v.ValueAsInt())
                        .ToList()
                })
                .OrderBy(r => r.Sum);

            if (m_Voting.VotingScheme.VotingType == VotingType.Score)
                return ratingRequest.Reverse().ToList();
            else
                return ratingRequest.ToList();

        }


    }
}
