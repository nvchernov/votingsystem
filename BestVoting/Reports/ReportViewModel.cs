﻿using CefSharp;
using DevExpress.Mvvm;
using Microsoft.Win32;
using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using VotingCore.Helpers;

namespace BestVoting.Reports
{
    public class ReportViewModel
    {
        private Report m_Report { get; }

        public ReportViewModel(Report report)
        {
            m_Report = report;
        }

        private string GetPatternString()
        {
            string result = null;

            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "BestVoting.Reports.RatingReportPattern.cshtml";

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                result = reader.ReadToEnd();
            }

            return result;
        }

        private string GetHtmlReport()
        {
            var result =
                Engine.Razor.RunCompile(GetPatternString(), "RatingReportPattern", model: m_Report);

            return result;
        }
        

        private ReportWindow wnd = null;

        public void Show()
        {
            wnd = new ReportWindow() { DataContext = this };

            wnd.Show();
        }

        #region PDF

        private DelegateCommand m_SaveToPdfFormatCommand;
        public DelegateCommand SaveToPdfFormatCommand { get => m_SaveToPdfFormatCommand ?? (m_SaveToPdfFormatCommand = new DelegateCommand(SaveAsPdf)); }
        private void SaveAsPdf()
        {
            var saveDialog = new SaveFileDialog();

            saveDialog.FileName = $"Отчет по голосованию ({m_Report.VotingType.Description()}) {m_Report.Name} {m_Report.Date.ToShortDateString()}.pdf";
            saveDialog.Filter = "pdf|*.pdf";

            //var report = GetHtmlReport();
            //var bytes = PdfSharpConvert(report);
            //File.WriteAllBytes(saveDialog.FileName, bytes);

            if (saveDialog.ShowDialog(wnd) == true)
                wnd.Browser.PrintToPdfAsync(saveDialog.FileName, new PdfPrintSettings());
            
        }

        private DelegateCommand m_ExitCommand;
        public DelegateCommand ExitCommand { get => m_ExitCommand ?? (m_ExitCommand = new DelegateCommand(Exit)); }
        private void Exit() => wnd.Close();

        //public static Byte[] PdfSharpConvert(string html)
        //{
        //    //CssData.
        //    byte[] res = null;
        //    using (var memoryStream = new MemoryStream())
        //    {
        //        var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(html, PdfSharp.PageSize.A4);
        //        pdf.Save(memoryStream);
        //        res = memoryStream.ToArray();
        //    }
        //    return res;
        //}

        #endregion 

        public void RenderReport()
        {
            if (wnd == null)
                return;

            var report = GetHtmlReport();

            wnd.Browser.LoadHtml(report, "http://example/", System.Text.Encoding.UTF8);

            
        }


    }
}
