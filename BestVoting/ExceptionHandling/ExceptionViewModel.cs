﻿using DevExpress.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestVoting.ExceptionHandling
{
    public class ExceptionViewModel : ViewModelBase
    {
        private readonly Exception exception;

        public ExceptionViewModel(Exception exception)
        {
            this.exception = exception;
            m_Message = exception.Message + Environment.NewLine + exception.StackTrace;
        }

        ExceptionWindow wnd;
        public bool? ShowDialog()
        {
            wnd = new ExceptionWindow() { DataContext = this };

            return wnd.ShowDialog();
        }


        private string m_Message;
        public string Message
        {
            get => m_Message;
            set
            {
                m_Message = value;
                RaisePropertyChanged("Message");
            }
        }

    }
}
