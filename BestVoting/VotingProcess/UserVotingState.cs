﻿using RLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VotingCore.DataModels;

namespace BestVoting.VotingProcesses
{
    public class UserVotingState
    {

        public UserVotingState(User user)
        {
            m_User = user;
        }

        private User m_User;
        public User User { get => m_User; set => m_User = value; }

        private List<Button> m_ButtonsHistory = new List<Button>();
        public List<Button> ButtonsHistory { get => m_ButtonsHistory; set => m_ButtonsHistory = value; }

        private List<string> m_SentText = new List<string>() { "" };
        public List<string> SentText { get => m_SentText; set => m_SentText = value; }

        /// <summary>
        /// Получить текст отправленный пультом по индексу
        /// </summary>
        /// <param name="index">Положительное или отрицательное значение индекса</param>
        public string GetSentTextByIndex(int index)
        {
            if (index >= 0)
            {
                return SentText[index];
            }
            else
            {
                if (SentText.Count == 0)
                    return null;
                else
                    return SentText[SentText.Count + index];
            }
        }

        public void AddToHistory(Button button)
        {
            if (button == null)
                return;

            var lastButton = ButtonsHistory.LastOrDefault();

            if (button.Number() && lastButton?.Number() == false)
                m_SentText.Add(button.ToString());
            else if (button.Number())
                m_SentText[m_SentText.Count - 1] += button.ToString();

            m_ButtonsHistory.Add(button);
        }

        public void Clear()
        {
            m_ButtonsHistory.Clear();
            m_SentText.Clear();
            m_SentText.Add("");
        }

        public int SentTextCount() => m_SentText.Count;

        public bool SendButtonWasSent() => m_ButtonsHistory.LastOrDefault()?.Kind == ButtonKind.Send;

        public string GetLastText() => m_SentText.Last();

        public string LastTextLenght() => m_SentText.Last();

        public ButtonKind? LastButtonKind() => m_ButtonsHistory?.LastOrDefault()?.Kind;
        
    }
}
