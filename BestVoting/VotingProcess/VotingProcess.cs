﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VotingCore.DataModels;
using RLib;
using VotingCore;
using System.Diagnostics;
using DevExpress.Mvvm;
using System.Collections.ObjectModel;
using BestVoting.VotingProcess;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace BestVoting.VotingProcesses
{
    /// <summary>
    /// Инкапсулирует процесс голосования
    /// </summary>
    public class VotingProcessViewModel : ViewModelBase
    {
        VotingDb ctx = new VotingDb();

        ReceiversManager receiverManager = new ReceiversManager();

        VotingState votingState;

        VotingScheme votingScheme;
        Voting voting;

        public VotingProcessViewModel(long votingSchemeId)
        {
            var votingScheme = ctx.VotingSchemes
                .Include(x => x.Members)
                .FirstOrDefault(x => x.Id == votingSchemeId);

            var voting = new Voting();
            voting.Name = votingScheme.Name;
            voting.Date = DateTime.Now;
            voting.VotingSchemeId = votingScheme.Id;
            voting.VotingScheme = votingScheme;

            ctx.Votings.Add(voting);

            ctx.SaveChanges();

            this.voting = voting;
            this.votingScheme = voting.VotingScheme;

            votingState = new VotingState(ctx.Users.ToList());

            receiverManager.ButtonClicked += ReceiverManager_ButtonClicked;
        }

        public EventHandler Started;
        private void OnStart() => Started?.Invoke(this, new EventArgs());

        private ObservableCollection<Vote> m_Votes = new ObservableCollection<Vote>();
        public ObservableCollection<Vote> Votes
        {
            get => m_Votes;
            set
            {
                m_Votes = value;
                RaisePropertyChanged("Votes");
            }
        }

        private DelegateCommand m_StartCommand;
        public DelegateCommand StartCommand { get => m_StartCommand ?? (m_StartCommand = new DelegateCommand(Start)); }
        public void Start()
        {
            receiverManager.Start();

            OnStart();
        }

        private DelegateCommand m_StopCommand;
        public DelegateCommand StopCommand { get => m_StopCommand ?? (m_StopCommand = new DelegateCommand(Stop)); }
        public void Stop()
        {
            receiverManager.Stop();
        }

        private VotingProcessWindow wnd;
        public bool? ShowDialog()
        {
            wnd = new VotingProcessWindow() { DataContext = this };
            return wnd.ShowDialog();
        }

        private void ReceiverManager_ButtonClicked(object sender, ButtonClickedEventArgs e)
        {
            try
            {

                var currentBtn = e.Button;
                var userState = votingState.GetByRemoteId(e.RemoteId);

                if (userState == null)
                {
                    e.SendbackCmd = SendbackCommand.DisplayStringClear("НЕ ЗАРЕГИСТРИРОВАН");
                    return;
                }

                if (currentBtn.Kind == ButtonKind.Send)
                {
                    if (userState.SentTextCount() >= 2 && userState.SentTextCount() % 2 == 0)
                    {
                        var memberNumber = Int32.TryParse(userState.GetSentTextByIndex(-2), out int num) ? num : 0;
                        var member = votingScheme.Members?.FirstOrDefault(x => x.Number == memberNumber);

                        if (member == null)
                        {
                            e.SendbackCmd = SendbackCommand.DisplayStringClear("УЧАСТИНКА\nНЕТ");
                            userState.Clear();
                            return;
                        }

                        //обработка ситуации, когда пользователь хочет заменить голос
                        Vote vote = Votes
                            .FirstOrDefault(x => x.Member?.Number == memberNumber && x.User.Id == userState.User.Id) ?? new Vote();

                        vote.ReceiverId = e.ReceiverId;
                        vote.RemoteId = e.RemoteId;
                        vote.VotingScheme = votingScheme;
                        vote.User = userState.User;
                        vote.Value = userState.GetSentTextByIndex(-1);
                        vote.Member = member;
                        vote.Voting = voting;

                        if (vote.Id == 0)
                            App.Current.Dispatcher.Invoke(() => Votes.Add(vote));
                        else
                            App.Current.Dispatcher.Invoke(() => { Votes.Remove(vote); Votes.Add(vote); });

                        ctx.Votes.AddOrUpdate(vote);
                        ctx.SaveChanges();

                        e.SendbackCmd = SendbackCommand.DisplayStringClear("ПРИНЯТО");
                    }
                    else
                    {
                        e.SendbackCmd = new SendbackCommand(e.ReceiverId, e.RemoteId, RemoteCommand.CMD_DISPLAY_CLEAR);
                    }
                }
                else if (currentBtn.Kind == ButtonKind.Esc)
                {
                    userState.Clear();
                    e.SendbackCmd = new SendbackCommand(e.ReceiverId, e.RemoteId, RemoteCommand.CMD_DISPLAY_CLEAR);
                }
                else if (currentBtn.Number())
                {
                    var lastText = userState.GetSentTextByIndex(-1);
                    if (string.IsNullOrWhiteSpace(lastText) || lastText.Length == 1)
                        e.SendbackCmd = SendbackCommand.AppendCharClear(currentBtn.ToChar());
                    else
                        e.SendbackCmd = SendbackCommand.AppendChar(currentBtn.ToChar());
                }

                userState.AddToHistory(currentBtn);

                //Debug.WriteLine($"нажата: {e.Button}, буфер: {userState.GetLastText()}");
            }
            catch (Exception ex)
            {

                try
                {
                    SendbackCommand.DisplayStringClear("ОШИБКА");
                    var currentBtn = e.Button;
                    var userState = votingState.GetByRemoteId(e.RemoteId);
                    userState.Clear();
                }
                catch(Exception ex1)
                {

                }
            }
        }

    }
}
