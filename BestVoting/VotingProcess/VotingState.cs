﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VotingCore.DataModels;

namespace BestVoting.VotingProcesses
{
    public class VotingState
    {
        private List<User> m_Users;
        public List<User> Users { get => m_Users; set => m_Users = value; }


        private List<UserVotingState> m_States;
        public List<UserVotingState> States { get => m_States; set => m_States = value; }

        public VotingState(List<User> users)
        {
            m_Users = users;
            m_States = m_Users.Select(x => new UserVotingState(x)).ToList();
        }

        public UserVotingState GetByRemoteId(int id)
        {
            return m_States.FirstOrDefault(x => x.User.RemoteId == id);
        }



    }
}
