﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace BestVoting
{
    /// <summary>
    /// Логика взаимодействия для Licence.xaml
    /// </summary>
    public partial class Licence : Window
    {
        public Licence()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Encoding enc = Encoding.GetEncoding(1252);

            var licence = File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "Licence.txt"), Encoding.UTF8);
            //var licenceEncoded = enc.GetString(Encoding.UTF8.GetBytes(licence));
            
            licenceTextBox.Text = licence;
        }
    }
}
