﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using DevExpress.Mvvm;
using VotingCore.DataModels;

namespace BestVoting.Users
{
    public class UserViewModel : ViewModelBase
    {
        public UserViewModel(User user)
        {
            m_User = user;
        }

        private User m_User;
        public User User
        {
            get => m_User;
            set
            {
                m_User = value;
                RaisePropertyChanged("User");
            }
        }


        UserWindow wnd = new UserWindow();
        public bool? ShowDialog()
        {
            wnd = new UserWindow() { DataContext = this };

            return wnd.ShowDialog();
        }


        private DelegateCommand m_OkCommand;
        public DelegateCommand OkCommand { get => m_OkCommand ?? (m_OkCommand = new DelegateCommand(Ok)); }

        private void Ok()
        {
            if(!Valid())
                return;

            wnd.DialogResult = true;
            wnd.Close();
        }

        private bool Valid()
        {
            if(string.IsNullOrWhiteSpace(m_User.Name))
            {
                MessageBox.Show("Имя не задано");
                return false;
            }

            return true;
        }

        private DelegateCommand m_CancelCommand;
        public DelegateCommand CancelCommand { get => m_CancelCommand ?? (m_CancelCommand = new DelegateCommand(Cancel)); }

        private void Cancel() => wnd?.Close();

    }







}
