﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Mvvm;
using VotingCore.DataModels;
using VotingCore;
using System.Windows;
using System.Data.Entity.Migrations;

namespace BestVoting.Users
{
    public class UsersListViewModel : ViewModelBase
    {
        UsersListWindow wnd = null;
        VotingDb ctx = new VotingDb();

        public void ShowDialog()
        {
            wnd = new UsersListWindow() { DataContext = this };

            Users = new ObservableCollection<User>(ctx.Users.ToList());

            wnd.ShowDialog();
        }

        private DelegateCommand m_CreateCommand;
        public DelegateCommand CreateCommand { get => m_CreateCommand ?? (m_CreateCommand = new DelegateCommand(Create)); }
        private void Create()
        {
            var user = new User();

            var userWnd = new UserViewModel(user);

            if (userWnd.ShowDialog() == true)
            {
                ctx.Users.Add(user);
                ctx.SaveChanges();

                Users.Add(user);
            }

        }

        private DelegateCommand m_EditCommand;
        public DelegateCommand EditCommand { get => m_EditCommand ?? (m_EditCommand = new DelegateCommand(Edit)); }
        private void Edit()
        {
            if (SelectedUser == null)
            {
                MessageBox.Show("Не выбран ни один элемент");
                return;
            }

            var user = (User)m_SelectedUser.Clone();

            var userWnd = new UserViewModel(user);

            if (userWnd.ShowDialog() == true)
            {
                ctx.Entry(m_SelectedUser).State = System.Data.Entity.EntityState.Detached;
                ctx.Users.AddOrUpdate(user);

                ctx.SaveChanges();

                Users.Remove(m_SelectedUser);
                Users.Add(user);
                m_SelectedUser = user;
            }

        }

        private DelegateCommand m_DeleteCommand;
        public DelegateCommand DeleteCommand { get => m_DeleteCommand ?? (m_DeleteCommand = new DelegateCommand(Delete)); }
        private void Delete()
        {
            if (SelectedUser == null)
            {
                MessageBox.Show("Не выбран ни один элемент");
                return;
            }

            if (MessageBox.Show("Вы действительно хотите удалить?", "Удаление", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                return;

            var editedUser = ctx.Entry(m_SelectedUser).State = System.Data.Entity.EntityState.Deleted;

            ctx.SaveChanges();

            Users.Remove(m_SelectedUser);

        }

        private ObservableCollection<User> m_Users;
        public ObservableCollection<User> Users
        {
            get => m_Users;
            set
            {
                m_Users = value;
                RaisePropertyChanged("Users");
            }
        }
        
        private User m_SelectedUser;
        public User SelectedUser
        {
            get => m_SelectedUser;
            set
            {
                m_SelectedUser = value;
                RaisePropertyChanged("SelectedUser");
            }
        }

    }
}
