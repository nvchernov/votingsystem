﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BestVoting.Members;
using BestVoting.Users;
using BestVoting.VotingSchemes;
using DevExpress.Mvvm;

namespace BestVoting
{
    public class MainViewModel : ViewModelBase
    {


        private DelegateCommand m_ShowUsersListCommand;
        public DelegateCommand ShowUsersListCommand { get => m_ShowUsersListCommand ?? (m_ShowUsersListCommand = new DelegateCommand(ShowUsersList)); }
        private void ShowUsersList()
        {
            var usersListViewModel = new UsersListViewModel();
            usersListViewModel.ShowDialog();
        }

        private DelegateCommand m_ShowMembersListCommand;
        public DelegateCommand ShowMembersListCommand { get => m_ShowMembersListCommand ?? (m_ShowMembersListCommand = new DelegateCommand(ShowMembersList)); }
        private void ShowMembersList()
        {
            var membersListViewModel = new MembersListViewModel();
            membersListViewModel.ShowDialog();
        }


        private DelegateCommand m_ShowVotingsListCommand;
        public DelegateCommand ShowVotingsListCommand { get => m_ShowVotingsListCommand ?? (m_ShowVotingsListCommand = new DelegateCommand(ShowVotingsList)); }
        private void ShowVotingsList()
        {
            var votingsListViewModel = new VotingSchemesListViewModel();
            votingsListViewModel.ShowDialog();
        }


        private DelegateCommand m_ShowLicenceCommand;
        public DelegateCommand ShowLicenceCommand { get => m_ShowLicenceCommand ?? (m_ShowLicenceCommand = new DelegateCommand(ShowLicence)); }
        private void ShowLicence()
        {
            Licence wnd = new Licence();
            wnd.ShowDialog();
        }

        MainWindow wnd = new MainWindow();
        public void ShowDialog()
        {
            wnd = new MainWindow() { DataContext = this };

            wnd.ShowDialog();
        }

    }
}
