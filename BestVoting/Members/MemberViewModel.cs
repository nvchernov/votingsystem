﻿using DevExpress.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using VotingCore.DataModels;
using VotingCore.Helpers;

namespace BestVoting.Members
{
    public class MemberViewModel : ViewModelBase
    {
        private readonly List<int> existingNumbers;

        private Member m_Member;
        public Member Member
        {
            get => m_Member;
            set
            {
                m_Member = value;
                RaisePropertyChanged("Member");
            }
        }

        public int memberNumber = 0; //используется для обновления 

        public MemberViewModel(Member member, List<int> existingNumbers = null)
        {
            m_Member = member;
            memberNumber = member.Number;
            this.existingNumbers = existingNumbers;
        }

        private ObservableCollection<Group> m_Groups;
        public ObservableCollection<Group> Groups
        {
            get => m_Groups;
            set
            {
                m_Groups = value;
                RaisePropertyChanged("Groups");
            }
        }

        public Group SelectedGroup
        {
            get => m_Member.Group;
            set
            {
                m_Member.Group = value;
                RaisePropertyChanged("SelectedGroup");
            }
        }

        MemberWindow wnd = new MemberWindow();
        public bool? ShowDialog()
        {
            wnd = new MemberWindow() { DataContext = this };

            return wnd.ShowDialog();
        }

        private DelegateCommand m_OkCommand;
        public DelegateCommand OkCommand { get => m_OkCommand ?? (m_OkCommand = new DelegateCommand(Ok)); }
        private void Ok()
        {
            if (!Valid())
                return;

            wnd.DialogResult = true;
            wnd.Close();
        }

        private bool Valid()
        {
            if(existingNumbers != null && existingNumbers.Count > 0)
            {
                if (m_Member.IsNew() && existingNumbers.Contains(m_Member.Number))
                {
                    MessageBox.Show("Такой номер уже существует в этой группе");
                    return false;
                }
                if (!m_Member.IsNew() && m_Member.Number != memberNumber)
                {
                    MessageBox.Show("Такой номер уже существует в этой группе");
                    return false;
                }
            }

            return true;
        }

        private DelegateCommand m_CancelCommand;
        public DelegateCommand CancelCommand { get => m_CancelCommand ?? (m_CancelCommand = new DelegateCommand(Cancel)); }
        private void Cancel() => wnd?.Close();

        private IEnumerable<KeyValuePair<Enum, string>> m_Ages;
        public IEnumerable<KeyValuePair<Enum, string>> Ages
        {
            get => m_Ages ?? (m_Ages = EnumExt.ConvertToKeyValueList(typeof(CatAge)));
            set
            {
                m_Ages = value;
                RaisePropertyChanged("Ages");
            }
        }

        private IEnumerable<KeyValuePair<Enum, string>> m_Sexes;
        public IEnumerable<KeyValuePair<Enum, string>> Sexes
        {
            get => m_Sexes ?? (m_Sexes = EnumExt.ConvertToKeyValueList(typeof(Sex)));
            set
            {
                m_Sexes = value;
                RaisePropertyChanged("Sexes");
            }
        }

    }
}
