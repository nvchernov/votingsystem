﻿using DevExpress.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using VotingCore;
using VotingCore.DataModels;

namespace BestVoting.Members
{
    public class MembersListViewModel : ViewModelBase
    {
        MembersListWindow wnd = null;
        VotingDb ctx = new VotingDb();

        public void ShowDialog()
        {
            wnd = new MembersListWindow() { DataContext = this };

            Members = new ObservableCollection<Member>(ctx.Members.Where(x => x.GroupId == 0).OrderBy(x => x.Number).ToList());

            Groups = new ObservableCollection<Group>(ctx.Groups.ToList());

            if (Groups.Count == 0)
            {
                var emptyGroup = new Group() { Name = "Без группы" };
                ctx.Groups.Add(emptyGroup);
                ctx.SaveChanges();
                Groups.Add(emptyGroup);
            }

            SelectedGroup = Groups.First();

            wnd.ShowDialog();
        }

        #region Members

        private DelegateCommand m_CreateCommand;
        public DelegateCommand CreateCommand { get => m_CreateCommand ?? (m_CreateCommand = new DelegateCommand(Create)); }
        private void Create()
        {
            var member = new Member() { Group = m_SelectedGroup };

            var existingNumbers = ctx.Members.Where(x => x.GroupId == m_SelectedGroup.Id).Select(x => x.Number).ToList();
            member.Number = existingNumbers?.Count > 0 ? existingNumbers.Max() + 1 : 0;

            var memberViewModel = new MemberViewModel(member, existingNumbers) { Groups = m_Groups };

            if (memberViewModel.ShowDialog() == true)
            {
                ctx.Members.Add(member);
                ctx.SaveChanges();

                Members.Add(member);
                Members = new ObservableCollection<Member>(Members.OrderBy(x => x.Number).ToList());
            }

        }

        private DelegateCommand m_EditCommand;
        public DelegateCommand EditCommand { get => m_EditCommand ?? (m_EditCommand = new DelegateCommand(Edit)); }
        private void Edit()
        {
            if (SelectedMember == null)
            {
                MessageBox.Show("Не выбран ни один элемент");
                return;
            }

            var member = (Member)m_SelectedMember.Clone();

            var existingNumbers = ctx.Members.Where(x => x.GroupId == m_SelectedGroup.Id).Select(x => x.Number).ToList();

            var memberWnd = new MemberViewModel(member, existingNumbers) { Groups = m_Groups };

            if (memberWnd.ShowDialog() == true)
            {
                ctx.Entry(m_SelectedMember).State = System.Data.Entity.EntityState.Detached;
                member.GroupId = member.Group?.Id ?? 0;
                ctx.Members.AddOrUpdate(member);

                ctx.SaveChanges();

                Members.Remove(m_SelectedMember);
                Members.Add(member);
                Members = new ObservableCollection<Member>(Members.OrderBy(x => x.Number).ToList());
                m_SelectedMember = member;
            }

        }

        private DelegateCommand m_DeleteCommand;
        public DelegateCommand DeleteCommand { get => m_DeleteCommand ?? (m_DeleteCommand = new DelegateCommand(Delete)); }
        private void Delete()
        {
            if (SelectedMember == null)
            {
                MessageBox.Show("Не выбран ни один элемент");
                return;
            }

            if (MessageBox.Show("Вы действительно хотите удалить?", "Удаление", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                return;

            ctx.Members.Remove(ctx.Members.FirstOrDefault(x => x.Id == m_SelectedMember.Id));

            ctx.SaveChanges();

            Members.Remove(m_SelectedMember);

        }

        private ObservableCollection<Member> m_Members;
        public ObservableCollection<Member> Members
        {
            get => m_Members;
            set
            {
                m_Members = value;
                RaisePropertyChanged("Members");
            }
        }

        private Member m_SelectedMember;
        public Member SelectedMember
        {
            get => m_SelectedMember;
            set
            {
                m_SelectedMember = value;
                RaisePropertyChanged("SelectedMember");
            }
        }

        #endregion

        #region Groups

        private Group m_SelectedGroup;
        public Group SelectedGroup
        {
            get => m_SelectedGroup;
            set
            {
                m_SelectedGroup = value;
                GroupChanged();
                RaisePropertyChanged("SelectedGroup");
            }
        }

        private void GroupChanged()
        {
            if(m_SelectedGroup != null)
                Members = new ObservableCollection<Member>(ctx.Members.Where(x => x.GroupId == m_SelectedGroup.Id).OrderBy(x => x.Number).ToList());
        }

        private ObservableCollection<Group> m_Groups;
        public ObservableCollection<Group> Groups
        {
            get => m_Groups;
            set
            {
                m_Groups = value;
                RaisePropertyChanged("Groups");
            }
        }

        private DelegateCommand m_CreateGroupCommand;
        public DelegateCommand CreateGroupCommand { get => m_CreateGroupCommand ?? (m_CreateGroupCommand = new DelegateCommand(CreateGroup)); }
        private void CreateGroup()
        {
            var group = new Group();

            var groupWnd = new GroupViewModel(group);

            if (groupWnd.ShowDialog() == true)
            {
                ctx.Groups.Add(group);
                ctx.SaveChanges();

                Groups.Add(group);
            }

        }

        private DelegateCommand m_EditGroupCommand;
        public DelegateCommand EditGroupCommand { get => m_EditGroupCommand ?? (m_EditGroupCommand = new DelegateCommand(EditGroup)); }
        private void EditGroup()
        {
            if (SelectedGroup == null)
            {
                MessageBox.Show("Не выбран ни один элемент");
                return;
            }

            var group = (Group)m_SelectedGroup.Clone();

            var groupWnd = new GroupViewModel(group);

            if (groupWnd.ShowDialog() == true)
            {
                ctx.Entry(m_SelectedGroup).State = System.Data.Entity.EntityState.Detached;
                ctx.Groups.AddOrUpdate(group);

                ctx.SaveChanges();

                Groups.Remove(m_SelectedGroup);
                Groups.Add(group);
                m_SelectedGroup = group;
            }

        }

        #endregion

    }
}
