﻿using DevExpress.Mvvm;
using System;
using System.Windows;
using VotingCore.DataModels;

namespace BestVoting.Members
{
    public class GroupViewModel : ViewModelBase
    {

        public GroupViewModel(Group group)
        {
            this.m_Group = group;
        }

        private Group m_Group;
        public Group Group
        {
            get => m_Group;
            set
            {
                m_Group = value;
                RaisePropertyChanged("Group");
            }
        }

        GroupWindow wnd = new GroupWindow();
        public bool? ShowDialog()
        {
            wnd = new GroupWindow() { DataContext = this };

            return wnd.ShowDialog();
        }

        private DelegateCommand m_OkCommand;
        public DelegateCommand OkCommand { get => m_OkCommand ?? (m_OkCommand = new DelegateCommand(Ok)); }
        private void Ok()
        {
            if (!Valid())
                return;

            wnd.DialogResult = true;
            wnd.Close();
        }

        private bool Valid()
        {
            if(string.IsNullOrWhiteSpace(m_Group.Name))
            {
                MessageBox.Show("Имя не задано");
                return false;
            }

            return true;
        }

        private DelegateCommand m_CancelCommand;
        public DelegateCommand CancelCommand { get => m_CancelCommand ?? (m_CancelCommand = new DelegateCommand(Cancel)); }
        private void Cancel() => wnd?.Close();
    }
}