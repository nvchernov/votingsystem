﻿using BestVoting.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace BestVoting
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {

        protected override void OnStartup(StartupEventArgs e)
        {
            this.Dispatcher.UnhandledException += Dispatcher_UnhandledException;

            base.OnStartup(e);

            var mvm = new MainViewModel();
            mvm.ShowDialog();
        }

        private void Dispatcher_UnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            ExceptionViewModel exceptionVM = new ExceptionViewModel(e.Exception);
            exceptionVM.ShowDialog();
        }
    }
}
