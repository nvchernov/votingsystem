﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VotingCore.DataModels
{
    /// <summary>
    /// Голсование
    /// </summary>
    public class VotingScheme : BaseEntity
    {
        public VotingScheme()
        {
            Date = DateTime.Now;
        }

        private VotingType votingType;
        public VotingType VotingType { get => votingType; set => votingType = value; }

        private List<Member> members;
        public virtual List<Member> Members { get => members; set => members = value; }

        private string name;
        public string Name { get => name; set => name = value; }

        private DateTime date;
        public DateTime Date { get => date; set => date = value; }

        public override string ToString()
        {
            return $"{votingType.AsString()} \"{Name}\"";
        }
    }

    /// <summary>
    /// Тип голосования
    /// </summary>
    public enum VotingType : int
    {
        /// <summary>
        /// Голосование в котором оценщик указывает место участника срдеи остальных
        /// </summary>
        [Description("Ранжирующие")]
        Ranking,

        /// <summary>
        /// Голосование в котором оценщик выставляет очки участинку
        /// </summary>
        [Description("Соревнование")]
        Score
    }

    public static class VotingTypeStringifier
    {
        public static string AsString(this VotingType votingType)
        {
            var res = string.Empty;

            switch(votingType)
            {
                case VotingType.Ranking:
                    res = "Ранжирующие голосование";
                    break;
                case VotingType.Score:
                    res = "Оценочное голосование";
                    break;
            }

            return string.IsNullOrWhiteSpace(res) ? "Голосование" : res;
        }




    }
}
