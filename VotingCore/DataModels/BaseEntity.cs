﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VotingCore.DataModels
{
    public class BaseEntity : ICloneable
    {
        protected long id;
        public long Id { get => id; set => id = value; }

        public object Clone() => this.MemberwiseClone();

        /// <summary>
        /// Сущности еще нет в БД
        /// </summary>
        public bool IsNew() => id == 0;

    }
}
