﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VotingCore.DataModels
{
    public class Voting : BaseEntity
    {
        private string m_Name;
        public string Name { get => m_Name; set => m_Name = value; }

        private DateTime m_Date;
        public DateTime Date { get => m_Date; set => m_Date = value; }

        private long m_VotingSchemeId;
        public long VotingSchemeId { get => m_VotingSchemeId; set => m_VotingSchemeId = value; }

        private VotingScheme m_VotingScheme;
        [ForeignKey("VotingSchemeId")]
        public VotingScheme VotingScheme { get => m_VotingScheme; set => m_VotingScheme = value; }

        private List<Vote> m_Votes;
        public virtual List<Vote> Votes { get => m_Votes; set => m_Votes = value; }

    }
}
