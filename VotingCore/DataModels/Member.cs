﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VotingCore.Helpers;

namespace VotingCore.DataModels
{
    /// <summary>
    /// Участник голосования
    /// </summary>
    public class Member : BaseEntity
    {

        #region FUTURE FEATRE

        private string name;
        public string Name { get => name; set => name = value; }

        private string lastName;
        public string LastName { get => lastName; set => lastName = value; }

        private string patronymic;
        public string Patronymic { get => patronymic; set => patronymic = value; }

        private List<VotingScheme> votingSchemes;
        public virtual List<VotingScheme> VotingSchemes { get => votingSchemes; set => votingSchemes = value; }

        private Int64 groupId;
        public Int64 GroupId { get => groupId; set => groupId = value; }

        private Group group;
        [ForeignKey("GroupId")]
        public Group Group { get => group; set => group = value; }

        #endregion

        private int number;
        public int Number { get => number; set => number = value; }

        private Sex sex;
        public Sex Sex { get => sex; set => sex = value; }

        public string SexAsString() => sex.Description();

        private CatAge catAge;
        public CatAge CatAge { get => catAge; set => catAge = value; }

        public string CatAgeAsString() => catAge.Description();

        private bool castrate = false;
        public bool Castrate { get => castrate; set => castrate = value; }



        //public override string ToString()
        //{
        //    if(!string.IsNullOrWhiteSpace(Name))
        //    {
        //        return $"{lastName} {name} {patronymic}".Trim();
        //    }
        //    else if(number != 0)
        //    {
        //        return number.ToString();
        //    }
        //    else
        //    {
        //        return id.ToString();
        //    }
        //}
    }

    public enum Sex : int
    {
        [Description("Мужской")]
        Male,
        [Description("Женский")]
        Female,
        [Description("Помет")]
        Litter
    }

    public enum CatAge : int
    {
        [Description("Котенок")]
        Kitty,
        [Description("Юниор")]
        Junior,
        [Description("Взрослый")]
        Adult,
        [Description("Ветеран")]
        Veteran,
        [Description("Помет")]
        Litter
    }

}
