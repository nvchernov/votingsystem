﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VotingCore.DataModels
{
    public class Group : BaseEntity
    {

        public Group()
        {
            CreatedAt = DateTime.Now;
        }

        private string name;
        public string Name { get => name; set => name = value; }

        private DateTime createdAt;
        public DateTime CreatedAt { get => createdAt; set => createdAt = value; }

    }
}
