﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VotingCore.DataModels
{
    /// <summary>
    /// Пользователь системы и человек проводящий голосования
    /// </summary>
    public class User : BaseEntity
    {
        private int remoteId;
        /// <summary>
        /// Пульт связаный с пользователем
        /// </summary>
        public int RemoteId { get => remoteId; set => remoteId = value; }

        private string name;
        public string Name { get => name; set => name = value; }

        private string lastName;
        public string LastName { get => lastName; set => lastName = value; }

        private string patronymic;
        public string Patronymic { get => patronymic; set => patronymic = value; }

        private string login;
        public string Login { get => login; set => login = value; }

        private string passwordHash;
        public string PasswordHash { get => passwordHash; set => passwordHash = value; }

        public override string ToString()
        {
            return $"{LastName} {Name} {Patronymic}".Trim();
        }
    }
}
