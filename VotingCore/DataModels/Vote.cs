﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VotingCore.DataModels
{
    /// <summary>
    /// Голос
    /// </summary>

    public class Vote : BaseEntity
    {
        public Vote()
        {
            Date = DateTime.Now;
        }

        private int remoteId;
        /// <summary>
        /// Hardware ID пульта с которого голосовали
        /// </summary>
        public int RemoteId { get => remoteId; set => remoteId = value; }

        private int dongleId;
        /// <summary>
        /// Hardware ID донгла с которого голосовали
        /// </summary>
        public int ReceiverId { get => dongleId; set => dongleId = value; }

        private DateTime date;
        public DateTime Date { get => date; set => date = value; }

        private string value;
        /// <summary>
        /// Значение голоса
        /// </summary>
        public string Value { get => value; set => this.value = value; }

        /// <summary>
        /// Пытается распарсить Value как число, при неудаче возвращает int.MinValue
        /// </summary>
        public int ValueAsInt() => int.TryParse(value, out int res) ? res : int.MinValue;

        private long m_VotingId;
        public long VotingId { get => m_VotingId; set => m_VotingId = value; }
        private Voting m_Voting;
        [ForeignKey("VotingId")]
        public Voting Voting { get => m_Voting; set => m_Voting = value; }

        private long votingSchemeId;
        public long VotingSchemeId { get => votingSchemeId; set => votingSchemeId = value; }
        private VotingScheme votingScheme;
        [ForeignKey("VotingSchemeId")]
        public VotingScheme VotingScheme { get => votingScheme; set => votingScheme = value; }

        private long? memberId;
        public long? MemberId { get => memberId; set => memberId = value; }
        private Member member;
        [ForeignKey("MemberId")]
        public Member Member { get => member; set => member = value; }

        private long userId;
        public long UserId { get => userId; set => userId = value; }
        private User user;
        [ForeignKey("UserId")]
        public User User { get => user; set => user = value; }
    }
}
