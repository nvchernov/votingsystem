﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SQLite;
using System.Data.SQLite.EF6;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VotingCore.DataModels;

namespace VotingCore
{
    public class VotingDb : DbContext
    {

        public VotingDb() : base("DefaultConnection")
        {
            Database.SetInitializer<VotingDb>(null);
        }

        public DbSet<Member> Members { get; set; }

        public DbSet<Group> Groups { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Vote> Votes { get; set; }

        public DbSet<VotingScheme> VotingSchemes { get; set; }

        public DbSet<Voting> Votings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<VotingScheme>()
                .HasMany(vg => vg.Members)
                .WithMany(m => m.VotingSchemes)
                .Map(m_vg => {
                    m_vg.MapLeftKey("VotingSchemeId");
                    m_vg.MapRightKey("MemberId");
                    m_vg.ToTable("VotingScheme_Member");
                });

        }

    }
}
