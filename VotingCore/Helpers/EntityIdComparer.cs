﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VotingCore.DataModels;

namespace VotingCore.Helpers
{
    public class EntityIdComparer : IEqualityComparer<BaseEntity>
    {
        public bool Equals(BaseEntity x, BaseEntity y)
        {
            if (x == null && y == null)
                return true;

            if (x == null && y != null || x != null && y == null)
                return false;

            return GetHashCode(x) == GetHashCode(y);
        }

        public int GetHashCode(BaseEntity obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
