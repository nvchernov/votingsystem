﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VotingCore.Helpers
{
    public static class EnumExt
    {
         /// <summary>
         /// Расширение для любого enum, позволяет получить описание поля помеченного атрибутом Description
         /// </summary>
        public static string Description(this Enum eValue)
        {
            var nAttributes = eValue.GetType().GetField(eValue.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (nAttributes.Any())
                return (nAttributes.First() as DescriptionAttribute).Description;

            TextInfo oTI = CultureInfo.CurrentCulture.TextInfo;
            return oTI.ToTitleCase(oTI.ToLower(eValue.ToString().Replace("_", " ")));
        }

        public static IEnumerable<KeyValuePair<Enum, string>> ConvertToKeyValueList(Type t)
        {
            if (!t.IsEnum)
                throw new ArgumentException("t must be an enum type");

            return Enum.GetValues(t).Cast<Enum>().Select((e) => new KeyValuePair<Enum, string>(e, e.Description())).ToList();
        }

        public static IEnumerable<KeyValuePair<Enum, string>> ConvertToKeyValueListWithEmpty(Type t)
        {
            var list = new List<KeyValuePair<Enum, string>>();
            list.Add(new KeyValuePair<Enum, string>(null, "Пусто"));
            list.AddRange(ConvertToKeyValueList(t));

            return list;
        }

    }
}
