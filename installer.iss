#define BitDepth "x86"
#define Target "Debug"

#define MyAppName "BestVoting"
#define MyAppVersion "1.0"
#define MyAppPublisher "Votum"
#define MyAppExeName "BestVoting.exe"


[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
PrivilegesRequired=admin
AppId={{06031CA4-DFB5-4F90-96CC-5B48A97686C6}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
DefaultDirName={pf}\{#MyAppName}
DisableProgramGroupPage=yes
LicenseFile=BestVoting\bin\{#BitDepth}\{#Target}\Licence.txt
OutputBaseFilename=BestVotingInstaller_{#BitDepth}
Compression=lzma
SolidCompression=yes

[Languages]
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "BestVoting\bin\{#BitDepth}\{#Target}\BestVoting.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "BestVoting\bin\{#BitDepth}\{#Target}\CefSharp.BrowserSubprocess.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "BestVoting\bin\{#BitDepth}\{#Target}\*.config"; DestDir: "{app}"; Flags: ignoreversion
Source: "BestVoting\bin\{#BitDepth}\{#Target}\*.bin"; DestDir: "{app}"; Flags: ignoreversion
Source: "BestVoting\bin\{#BitDepth}\{#Target}\*.pak"; DestDir: "{app}"; Flags: ignoreversion
Source: "BestVoting\bin\{#BitDepth}\{#Target}\*.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "BestVoting\bin\{#BitDepth}\{#Target}\*.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "BestVoting\bin\{#BitDepth}\{#Target}\Licence.txt"; DestDir: "{app}"; Flags: ignoreversion
Source: "BestVoting\bin\{#BitDepth}\{#Target}\bootstrap.min.css"; DestDir: "{app}"; Flags: ignoreversion
Source: "credits.txt"; DestDir: "{app}"; Flags: ignoreversion

Source: "BestVoting\db.db"; DestDir: "{app}"; Flags: ignoreversion

Source: "BestVoting\bin\{#BitDepth}\{#Target}\locales\*"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "BestVoting\bin\{#BitDepth}\{#Target}\Resources\*"; DestDir: "{app}\Resources"; Flags: ignoreversion
Source: "BestVoting\bin\{#BitDepth}\{#Target}\x64\*"; DestDir: "{app}\x64"; Flags: ignoreversion
Source: "BestVoting\bin\{#BitDepth}\{#Target}\x86\*"; DestDir: "{app}\x86"; Flags: ignoreversion

Source: "Deploy\NDP471-KB4033342-x86-x64-AllOS-ENU.exe"; DestDir: {tmp}; Flags: deleteafterinstall; AfterInstall: InstallFramework; Check: FrameworkIsNotInstalled


[Icons]
Name: "{commonprograms}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: runascurrentuser nowait postinstall skipifsilent

[Code]
function FrameworkIsNotInstalled: Boolean;
begin
  Result := not RegKeyExists(HKEY_LOCAL_MACHINE, 'SOFTWARE\Microsoft\.NETFramework\policy\v4.0');
end;

procedure InstallFramework;
var
  ResultCode: Integer;
begin
  if not Exec(ExpandConstant('{tmp}\dotNetFx40_Full_x86_x64.exe'), '/q /noreboot', '', SW_SHOW, ewWaitUntilTerminated, ResultCode) then
  begin
    MsgBox('.NET installation failed with code: ' + IntToStr(ResultCode) + '.',
      mbError, MB_OK);
  end;
end;

